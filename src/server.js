const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

const redisClient = require('../services/redisService');


const PROTO_PATH = __dirname + '/../protos/action.proto';
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true,
        bytes: String,
        arrays: true
    });
const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);
const host = "0.0.0.0";
const port = 50051;
const gestureServer = new protoDescriptor.action_module.Greeter(`${host}:50052`, grpc.credentials.createInsecure());
let userInput = {};
let response = {};



let Analyze = (call, cb) => {
    // console.log('Invoked');
    call.on('data', (chunk) => {
	    userInput = chunk;
    });

    call.on('end', () => {
        // console.log('end');
        const request_iterator = userInput.frame;
        const gesture_call = gestureServer.Analyze((err, result) => {
            if(err) {
                console.log(err);
            } else {
                console.log(result);
                // response = result;
                cb(null, {action: result.action, confidence: result.confidence});
            }
            console.log('returned');

        });

        setTimeout(() => {
            gesture_call.write({frame: request_iterator});
            // cb(null, {action: response.action, confidence: response.confidence});
        }, 50);
    });
};

let main = () => {
    const server = new grpc.Server();
    server.addService(protoDescriptor.action_module.Greeter.service, {Analyze: Analyze});
    server.bind(`${host}:${port}`, grpc.ServerCredentials.createInsecure());
    server.start();
    redisClient.assignListeners();
    console.log(gestureServer);
};

main();
