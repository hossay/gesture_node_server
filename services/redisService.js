const redis = require('redis');


let instance = null;


class RedisService {
    constructor() {
        this.client = null;

        if(!instance) {
            instance = this;
            this.client = redis.createClient();

            this.assignListeners = this.assignListeners.bind(this);
            this.checkUser = this.checkUser.bind(this);
        }

        return instance;
    }

    assignListeners() {
        const { client } = this;

        client.on('error', (err) => {
            console.log(`An error has occurred: ${err.toLocaleString()}`);
        });

        client.on('connect', () => {
            console.log('Redis client has connected with redis server(localhost:6379).');
        });

        client.on('end', () => {
            console.log('Redis client has closed.');
        });
    }

    checkUser(token) {
        const { client } = this;

        return client.hget('users', token, (err, user) => {
            if(err) {
                return console.log(`(checkUser(token))An error has occurred: ${err}`);
            }

            if(!user) {

            }
        });
    }
}

const redisService = new RedisService();


module.exports = redisService;